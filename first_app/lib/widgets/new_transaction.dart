import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NewTransaction extends StatefulWidget {
  final Function addtx;

  NewTransaction(this.addtx);

  @override
  State<NewTransaction> createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final _titleController = TextEditingController();
  final _amountController = TextEditingController();
  DateTime? _selectedDate;
  void _submitData() {
    final enteredTitle = _titleController.text;
    final enteredAmount = double.parse(_amountController.text);

    if (enteredTitle.isNotEmpty && enteredAmount > 0 || _selectedDate != null) {
      widget.addtx(enteredTitle, enteredAmount, _selectedDate);
      Navigator.of(context).pop();
    } else {
      print("vacio completar");
      return;
    }
  }

  void _presentDatePicker() {
    Platform.isIOS
        ? showModalBottomSheet<DateTime>(
            context: context,
            builder: (context) {
              DateTime? tempPickedDate;
              return Container(
                height: 250,
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          CupertinoButton(
                            child: Text('Cancel'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          CupertinoButton(
                            child: Text('Done'),
                            onPressed: () {
                              Navigator.of(context).pop(tempPickedDate);
                            },
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 0,
                      thickness: 1,
                    ),
                    Expanded(
                      child: Container(
                        child: CupertinoDatePicker(
                          mode: CupertinoDatePickerMode.date,
                          initialDateTime: DateTime.now(),
                          minimumDate: DateTime(2019),
                          maximumDate: DateTime.now(),
                          onDateTimeChanged: (DateTime dateTime) {
                            tempPickedDate = dateTime;
                            if (dateTime == null) {
                              return;
                            }
                            setState(() {
                              _selectedDate = dateTime;
                            });
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          )
        : showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime(2019),
                lastDate: DateTime.now())
            .then((value) {
            if (value == null) {
              return;
            }
            setState(() {
              _selectedDate = value;
            });
          });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Card(
        elevation: 5,
        child: Container(
          padding: EdgeInsets.only(
              top: 10,
              left: 10,
              right: 10,
              bottom: MediaQuery.of(context).viewInsets.bottom + 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TextField(
                decoration: InputDecoration(labelText: 'Title'),
                onSubmitted: (_) => _submitData(),
                controller: _titleController,
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Amount'),
                onSubmitted: (_) => _submitData(),
                keyboardType: TextInputType.number,
                controller: _amountController,
              ),
              Container(
                height: 70,
                child: Row(
                  children: [
                    Expanded(
                      child: Text(_selectedDate == null
                          ? 'No date Chosen!'
                          : 'Picked Date: ${DateFormat.yMd().format(_selectedDate!)}'),
                    ),
                    Platform.isIOS
                        ? CupertinoButton(
                            child: Text(
                              'Choose Date',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            onPressed: _presentDatePicker)
                        : TextButton(
                            style: TextButton.styleFrom(
                                textStyle: TextStyle(
                                    color: Theme.of(context).primaryColor)),
                            child: Text(
                              'Choose Date',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            onPressed: _presentDatePicker,
                          )
                  ],
                ),
              ),
              ElevatedButton(
                child: Text('Add Transaction'),
                style: TextButton.styleFrom(
                  backgroundColor: Theme.of(context).primaryColor,
                  foregroundColor: Colors.white,
                ),
                onPressed: _submitData,
              )
            ],
          ),
        ),
      ),
    );
  }
}
