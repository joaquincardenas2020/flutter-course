import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../models/transaction.dart';
import 'package:intl/intl.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final Function deleteTx;
  TransactionList(this.transactions, this.deleteTx);

  checkEmptyWidget(BuildContext context) {
    if (transactions.isNotEmpty) {
      return ListView.builder(
        itemBuilder: (ctx, index) {
          return Card(
            elevation: 5,
            margin: EdgeInsets.symmetric(
              vertical: 8,
              horizontal: 5,
            ),
            child: ListTile(
              leading: CircleAvatar(
                radius: 30,
                child: Padding(
                    padding: EdgeInsets.all(6),
                    child: FittedBox(
                        child: Text('\$${transactions[index].amount}'))),
              ),
              title: Text(transactions[index].title,
                  style: Theme.of(context).textTheme.headline6),
              subtitle:
                  Text(DateFormat.yMMMd().format(transactions[index].date)),
              trailing: MediaQuery.of(context).size.width > 460
                  ? TextButton.icon(
                      onPressed: () {
                        deleteTx(transactions[index].id);
                      },
                      style: TextButton.styleFrom(
                        primary: Theme.of(context).errorColor,
                      ),
                      icon: Icon(Icons.delete),
                      label: Text('Delete'))
                  : IconButton(
                      icon: Icon(Icons.delete),
                      color: Theme.of(context).errorColor,
                      onPressed: () {
                        deleteTx(transactions[index].id);
                      },
                    ),
            ),
          );
        },
        itemCount: transactions.length,
      );
    } else {
      return LayoutBuilder(builder: (ctx, constraints) {
        return Column(
          children: [
            Text(
              "No transactions added yet",
              style: Theme.of(context).textTheme.headline6,
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0.0, 20, 0.0, 0.0),
              height: constraints.maxHeight * 0.6,
              child: Image.asset(
                "assets/images/453-4535967_zzz-transparent-white-transparent-z-sleep-hd-png.png",
                fit: BoxFit.cover,
              ),
            )
          ],
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return checkEmptyWidget(context);
  }
}
